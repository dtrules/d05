<%--
 * display.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<p><spring:message code="finder.display" /></p>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="finder.display"/> </h2>

<spring:message code="finder.date.pattern" var="datePattern"/>

	<!-- Attributes -->
	
	<b><spring:message code="finder.moment"/></b>: <fmt:formatDate value="${finder.moment}" pattern="${datePattern}" />
	<br/>
	<b><spring:message code="finder.keyWord"/></b>: <jstl:out value="${finder.keyWord}" />
	<br/>
	<b><spring:message code="finder.minPrice"/></b>: <jstl:out value="${finder.minPrice}"/>
    <br/>
    <b><spring:message code="finder.maxPrice"/></b>: <jstl:out value="${finder.maxPrice}"/>
    <br/>
	<b><spring:message code="finder.startMoment"/></b>: <fmt:formatDate value="${finder.startMoment}" pattern="${datePattern}" />
    <br/>
    <b><spring:message code="finder.endMoment"/></b>: <fmt:formatDate value="${finder.endMoment}" pattern="${datePattern}" />
    <br/>
	<b><spring:message code="finder.category"/></b>: <jstl:out value="${finder.category}"/>
	<br/>
	<b><spring:message code="finder.warranty"/></b>: <jstl:out value="${finder.warranty}"/>
	<br/>

	<!--New search-->
	<input type="button" name="create" onclick="javascript: window.location.replace('finder/handyWorker/edit.do')"
		value="<spring:message code="finder.newSearch" />" />
		
	<!--Task results-->
	<display:table name="tasks" id="row" requestURI="${requestURI}" pagesize="10" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila (handyWorker)-->

	<security:authorize access="hasRole{'HANDYWORKER'}">
	<display:column>
		<a href="application/handyWorker/display.do?taskId=${row.id}">
			<spring:message code="task.display"/>
		</a>
	</display:column>
	</security:authorize>
	
	<display:column property="ticker" titleKey="task.ticker" sortable="true"/>
	<display:column property="publishDate" titleKey="task.publishDate" sortable="true" format="{0,date,dd/mm/yyyy HH:mm}"/>
	<display:column property="description" titleKey="task.description" sortable="false"/>
	<display:column property="address" titleKey="task.address" sortable="true"/>
	<display:column property="maxPrice" titleKey="task.maxPrice" sortable="true"/>
	<display:column property="startMoment" titleKey="task.startMoment" sortable="true"/>
	<display:column property="endMoment" titleKey="task.endMoment" sortable="true"/>
	<display:column property="warranty" titleKey="task.warranty" sortable="true"/>
	<display:column property="category" titleKey="task.category" sortable="true"/>
	
</display:table>
	
</html>