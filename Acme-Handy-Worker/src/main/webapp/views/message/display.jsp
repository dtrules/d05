<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<p><spring:message code="message.display" /></p>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="message.display"/> </h2>

<spring:message code="message.date.pattern" var="datePattern"/>
	
	<!-- Attributes -->
	
	<b><spring:message code="message.moment"/></b>: <fmt:formatDate value="${message.moment}" pattern="${datePattern}" />
	<br/>
	<b><spring:message code="message.sender"/></b>: <jstl:out value="${message.sender}"/>
    <br/>
    <b><spring:message code="message.recipient"/></b>: <jstl:out value="${message.recipient}"/>
    <br/>
	<b><spring:message code="message.subject"/></b>: <jstl:out value="${message.subject}"/>
    <br/>
    <b><spring:message code="message.body"/></b>: <jstl:out value="${message.body}"/>
    <br/>
	<b><spring:message code="message.priority"/></b>: <jstl:out value="${message.priority}"/>
	<br/>
	<b><spring:message code="message.tags"/></b>: <jstl:out value="${message.tags}"/>
	<br/>

	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('message/list.do')"
		value="<spring:message code="application.cancel" />" />	

</html>
	
