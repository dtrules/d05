<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="message.create" /></p>

<security:authorize access="isAuthenticated()">

<form:form action="message/create.do" modelAttribute="message">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />

    <!--Recipient-->
    <form:label path="recipient">
        <spring:message code="message.recipient"/>
        </form:label>
    <form:select id="recipients" path="recipient">
        <form:options items="${recipients}" itemLabel="value" itemValue="id"/>
        <form:option value="0" label="------"/>
    </form:select>
    <form:errors cssClass="error" path="recipient"/>
    <br/>

	<!--Subject-->
	<form:label path="subject">
		<spring:message code="message.subject"/>
    	</form:label>
	<form:input path="subject"/>
    	<form:errors cssClass="error" path="subject" />

    <!--Body-->
    <form:label path="body">
        <spring:message code="message.body"/>
        </form:label>
    <form:input path="body"/>
        <form:errors cssClass="error" path="body" />

    <!--Priority-->
    <form:label path="priority">
        <spring:message code="message.priority"/>
        </form:label>
    <form:select id="priorities" path="priority">
        <form:options items="${priorities}" itemLabel="value" itemValue="id"/>
        <form:option value="0" label="------"/>
    </form:select>
    <form:errors cssClass="error" path="priority"/>
    <br/>

    <!--Tags-->
    <form:label path="tags">
        <spring:message code="message.tags"/>
        </form:label>
    <form:textarea path="tags"/>
        <form:errors cssClass="error" path="tags" />

	
	<input type="submit" name="save" value="<spring:message code="message.save"/>"/>

	<input type="submit" name="cancel" value="<spring:message code="message.cancel"/>"/>
	<br/>

</form:form>
</security:authorize>
