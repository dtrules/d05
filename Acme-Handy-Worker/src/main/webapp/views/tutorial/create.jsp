<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="tutorial.create" /></p>

<security:authorize access="hasRole('HANDYWORKER')">

<form:form action="tutorial/handyWorker/create.do" modelAttribute="tutorial">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
    <form:hidden path="moment" />  
    


	<!--Title-->
	<form:label path="title">
		<spring:message code="tutorial.title"/>
    	</form:label>
	<form:input path="title"/>
    	<form:errors cssClass="error" path="title" />

	

	<!--Summary-->
	<form:label path="summary">
		<spring:message code="tutorial.summary"/>
    	</form:label>
	<form:input path="summary"/>
    	<form:errors cssClass="error" path="summary" />
    	
    	
    <!--Pictures-->
	<form:label path="picture">
		<spring:message code="tutorial.pictures"/>
    	</form:label>
	<form:textarea path="pictures"/>
    	<form:errors cssClass="error" path="picture" />
    	
    	
    	
    <!--Section-->
    <form:label path="section">
        <spring:message code="tutorial.section"/>
        </form:label>
    <form:select id="sections" path="section">
        <form:options items="${sections}" itemLabel="value" itemValue="id"/>
        <form:option value="0" label="------"/>
    </form:select>
    <form:errors cssClass="error" path="section"/>
    <br/>	
	
	<input type="submit" name="save" value="<spring:message code="tutorial.save"/>"/>

<input type="button" name="cancel"
		value="<spring:message code="tutorial.cancel" />"
		onclick="javascript: relativeRedir('tutorial/list.do');" />	

</form:form>
</security:authorize>
