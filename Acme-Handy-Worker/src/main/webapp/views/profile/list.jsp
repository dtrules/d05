<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p>
	<spring:message code="profile.list" />
</p>

<!--Tabla-->

<display:table name="profiles" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar y ver en cada fila-->
	<security:authorize access="isAuthenticated()">
		<display:column>
			<a href="profile/edit.do?profileId=${row.id}"> <spring:message
					code="profile.edit" />
			</a>
		</display:column>

		<display:column>
			<a href="profile/display.do?profileId=${row.id}"> <spring:message
					code="profile.display" />
			</a>
		</display:column>

		<spring:message code="profile.nick" var="nickHeader" />
		<display:column property="nick" title="${nickHeader}" sortable="true" />

		<spring:message code="profile.socialNetwork" var="socialNetworkHeader" />
		<display:column property="socialNetwork"
			title="${socialNetworkHeader}" sortable="true" />

		<spring:message code="profile.link" var="linkHeader" />
		<display:column property="link" title="${linkHeader}" sortable="true" />
	</security:authorize>

</display:table>

<!--Bot�n de crear debajo de la lista-->
<security:authorize access="isAuthenticated()">
	<div>
		<a href="profile/create.do"> <spring:message
				code="profile.create" />
		</a>
	</div>
</security:authorize>