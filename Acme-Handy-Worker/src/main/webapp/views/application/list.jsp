<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="application.list" /></p>

<!--Tabla-->

<display:table name="applications" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila (handyWorker)-->

	<security:authorize access="hasRole{'HANDYWORKER'}">
	<display:column>
		<a href="application/handyWorker/edit.do?applicationId=${row.id}">
			<spring:message code="application.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="application/handyWorker/display.do?applicationId=${row.id}">
			<spring:message code="application.display"/>
		</a>
	</display:column>
	</security:authorize>
	<display:column property="moment" titleKey="application.moment" sortable="true" format="{0,date,dd/mm/yyyy HH:mm}"/>
	<display:column property="status" titleKey="application.status" sortable="true"/>
	<display:column property="price" titleKey="application.price" sortable="true"/>
	
	<!-- Bot�n de aceptar y denegar la aplicacion (customer) -->
	<security:authorize access="hasRole{'CUSTOMER'}">
		<display:column>
		<a href="application/customer/accept.do?applicationId=${row.id}">
			<spring:message code="application.accept"/>
		</a>
	</display:column>
		<display:column>
		<a href="application/customer/reject.do?applicationId=${row.id}">
			<spring:message code="application.reject"/>
		</a>
	</display:column>
	</security:authorize>
	
</display:table>



<!--Bot�n de crear debajo de la lista-->

<security:authorize access="hasRole{'HANDYWORKER'}">
	<div>
		<a href="application/handyWorker/create.do">
			<spring:message code="application.create"/>
		</a>
	</div>
</security:authorize>









