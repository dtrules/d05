<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="application.edit" /></p>

<security:authorize access="hasRole('HANDYWORKER')">

<form:form action="application/handyWorker/edit.do" modelAttribute="application" >

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
	<form:hidden path="moment" />  


	<!--Status-->
	<form:label path="status">
		<spring:message code="application.status"/>
    	</form:label>
	<form:select id="statuses" path="status">
		<form:options items="${statuses}" itemLabel="name" itemValue="id"/>
		<form:option value="0" label="------"/>
	</form:select>
	<form:errors cssClass="error" path="status"/>
	<br/>

	<!--Price-->
	<form:label path="price">
		<spring:message code="application.price"/>
    	</form:label>
	<form:input path="price"/>
    	<form:errors cssClass="error" path="price" />

	
	<!--Comments-->
	<form:label path="comments">
		<spring:message code="application.comments"/>
    	</form:label>
	<form:textarea path="comments"/>
    	<form:errors cssClass="error" path="comments" />

	<!--  Botones de save y cancelar -->
	
	<input type="submit" name="save" value="<spring:message code="application.save"/>"/>
	
	<input type="button" name="cancel"
		value="<spring:message code="application.cancel" />"
		onclick="javascript: relativeRedir('application/customer/list.do');" />
	
	

</form:form>
</security:authorize>
