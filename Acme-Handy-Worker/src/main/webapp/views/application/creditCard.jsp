<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="application.edit" /></p>

<security:authorize access="hasRole('CUSTOMER')">

<form:form action="application/customer/creditCard.do" modelAttribute="application" >


	<!-- HolderName-->
    	<form:label path="holderName">
		<spring:message code="creditCard.holderName"/>
    	</form:label>
    	<form:input path="holderName" />
    	<form:errors cssClass="error" path="holderName" />
	<br/>
	
		<!-- BrandName-->
    	<form:label path="brandName">
		<spring:message code="creditCard.brandName"/>
    	</form:label>
    	<form:input path="brandName" />
    	<form:errors cssClass="error" path="brandName" />
	<br/>
	
		<!-- Number-->
    	<form:label path="number">
		<spring:message code="creditCard.number"/>
    	</form:label>
    	<form:input path="number" />
    	<form:errors cssClass="error" path="number" />
	<br/>
	
		<!-- Expiration Month-->
    	<form:label path="expirationMonth">
		<spring:message code="creditCard.expirationMonth"/>
    	</form:label>
    	<form:input path="expirationMonth" />
    	<form:errors cssClass="error" path="expirationMonth" />
	<br/>
	
		<!-- Expiration Year-->
    	<form:label path="expirationYear">
		<spring:message code="creditCard.expirationYear"/>
    	</form:label>
    	<form:input path="expirationYear" />
    	<form:errors cssClass="error" path="expirationYear" />
	<br/>
	
			<!-- CVVCode-->
    	<form:label path="cvvCode">
		<spring:message code="creditCard.cvvCode"/>
    	</form:label>
    	<form:input path="cvvCode" />
    	<form:errors cssClass="error" path="cvvCode" />
	<br/>

	

	<!--  Botones de save y cancelar -->
	
	<input type="submit" name="save" value="<spring:message code="application.save"/>"/>
	
	<input type="button" name="cancel"
		value="<spring:message code="application.cancel" />"
		onclick="javascript: relativeRedir('application/customer/list.do');" />
	
	

</form:form>
</security:authorize>
