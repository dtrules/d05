<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="complaint.list" /></p>

<!--Tabla-->

<display:table name="complaints" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->

	<display:column>
		<a href="complaint/display.do?complaintId=${row.id}">
			<spring:message code="complaint.display"/>
		</a>
	</display:column>
	
	<spring:message code="complaint.ticker" var="tickerHeader"/>
	<display:column property="ticker" title="${tickerHeader}" sortable="true" />
	
	
	<spring:message code="complaint.moment" var="momentHeader"/>
	<display:column property="moment" title="${momentHeader}" sortable="true" />
	
	<spring:message code="complaint.description" var="descriptionHeader"/>
	<display:column property="description" title="${descriptionHeader}" sortable="true" />
	
	
</display:table>



<!--Bot�n de crear debajo de la lista-->

<security:authorize access="hasRole('CUSTOMER')">
	<div>
		<a href="complaint/customer/create.do">
			<spring:message code="complaint.create"/>
		</a>
	</div>
</security:authorize>
