<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="complaint.edit" /></p>

<security:authorize access="hasRole('CUSTOMER')">

<form:form action="complaint/customer/edit.do" modelAttribute="complaint">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
    <form:hidden path="ticker" /> 
    <form:hidden path="moment" /> 


		<!--Description-->
	<form:label path="description">
		<spring:message code="complaint.description"/>
    	</form:label>
	<form:textarea path="description" type="text"/>
    	<form:errors cssClass="error" path="description" />
    	
    	
    <!--Attachments-->
	<form:label path="attachment">
		<spring:message code="complaint.attachments"/>
    	</form:label>
	<form:textarea path="attachment"/>
    	<form:errors cssClass="error" path="attachment" />
	
	<input type="submit" name="save" value="<spring:message code="complaint.save"/>"/>

	<input type="button" name="cancel"
		value="<spring:message code="complaint.cancel" />"
		onclick="javascript: relativeRedir('complaint/list.do');" />

</form:form>
</security:authorize>
