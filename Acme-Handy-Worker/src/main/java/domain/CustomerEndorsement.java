
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class CustomerEndorsement extends Endorsement {

	// Attributes

	private double		score;			//(derivada)

	private HandyWorker	handyWorker;


	// Getters & setters

	public double getScore() {
		return this.score;
	}

	public void setScore(final double score) {
		this.score = score;
	}

	// Relationships

	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public HandyWorker getHandyWorker() {
		return this.handyWorker;
	}

	public void setHandyWorker(final HandyWorker handyWorker) {
		this.handyWorker = handyWorker;
	}

}
