
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.HandyWorkerEndorsement;

@Repository
public interface HandyWorkerEndorsementRepository extends JpaRepository<HandyWorkerEndorsement, Integer> {

	@Query("select he from HandyWorkerEndorsement he where he.customer.id = ?1")
	Collection<HandyWorkerEndorsement> getEndorsementsFromCustomer(final int customerId);
}
