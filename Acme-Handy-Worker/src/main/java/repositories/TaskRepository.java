
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Application;
import domain.Customer;
import domain.Finder;
import domain.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

	@Query("select f from Finder f join f.tasks t where t.id = ?1")
	Collection<Finder> finderByTask(int taskId);

	@Query("select c from Customer c join c.tasks t where t.id=?1")
	Customer customerByTask(int taskId);

	@Query("select a from Application a where a.task.id =?1")
	Collection<Application> applicationByTask(int taskId);

	@Query("select avg(t.applications.size), min(t.applications.size), max(t.applications.size), stddev(t.applications.size) from Task t")
	Collection<Double> avgMinMaxStddevApplicationsPerTask();

	@Query("select avg(maxPrice), min(maxPrice), max(maxPrice), stddev(maxPrice) from Task t")
	Collection<Double> avgMinMaxStddevMaxPricePerTask();

	@Query("select min(t.complaints.size), max(t.complaints.size), avg(t.complaints.size), stddev(t.complaints.size) from Task t")
	Collection<Double> avgMinMaxStddevComplaintsPerTask();

	@Query("select count(t)*1.0/(select count(t1) from Task t1) from Task t where t.complaints.size >= 1")
	Double ratioTasksWithAComplaint();
}
