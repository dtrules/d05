
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query("select c from Customer c where c.userAccount.username = ?1")
	Customer findCustomerByUsername(String username);

	@Query("select c from Customer c where c.userAccount = ?1")
	Customer findByUserAccount(UserAccount userAccount);

	@Query("select c from Customer c join c.tasks t where t.publishDate < CURRENT_TIME and c.tasks.size >= (select avg(t2) from Task t2 where t2.publishDate < CURRENT_TIME)+((select avg(t3) from Task t3 where t3.publishDate < CURRENT_TIME)*0.1) order by c.tasks.size")
	Collection<Customer> customerListPublishedTasks();

	@Query("select c from Customer c join c.tasks t order by t.complaints.size")
	Collection<Customer> top3CustomerByComplaint();

	@Query("select avg(c.tasks.size), min(c.tasks.size), max(c.tasks.size), stddev(c.tasks.size) from Customer c")
	Collection<Double> avgMinMaxStddevTasksPerCustomer();

}
