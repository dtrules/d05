
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ProfessionalRecordRepository;
import domain.ProfessionalRecord;

@Service
@Transactional
public class ProfessionalRecordService {

	//Managed repository ---------------------------------------------------

	@Autowired
	private ProfessionalRecordRepository	professionalRecordRepository;

	//Supporting Services--------------------------------------------------

	@Autowired
	private ActorService					actorService;


	// Contructor methods
	public ProfessionalRecordService() {
		super();
	}

	public ProfessionalRecord create() {
		final ProfessionalRecord res = new ProfessionalRecord();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		Assert.notNull(res);

		return res;
	}

	public Collection<ProfessionalRecord> findAll() {
		final Collection<ProfessionalRecord> res = this.professionalRecordRepository.findAll();

		Assert.notNull(res);

		return res;

	}

	public ProfessionalRecord findOne(final int professionalRecordId) {
		Assert.isTrue(professionalRecordId > 0);

		final ProfessionalRecord res = this.professionalRecordRepository.findOne(professionalRecordId);

		Assert.notNull(res);

		return res;
	}

	public ProfessionalRecord save(final ProfessionalRecord professionalRecord) {
		Assert.notNull(professionalRecord);
		final Date fechaActual = new Date();
		this.checkProfessionalRecord(professionalRecord);
		this.actorService.checkSpamWords(professionalRecord.getAttachment());
		Assert.isTrue((professionalRecord.getStartMoment().before(fechaActual)));
		Assert.isTrue((professionalRecord.getStartMoment().before(professionalRecord.getEndMoment())));
		for (final String s : professionalRecord.getComments())
			this.actorService.checkSpamWords(s);
		this.actorService.checkSpamWords(professionalRecord.getCompany());
		this.actorService.checkSpamWords(professionalRecord.getRole());

		final ProfessionalRecord res = this.professionalRecordRepository.save(professionalRecord);

		return res;
	}

	public void delete(final ProfessionalRecord professionalRecord) {
		Assert.notNull(professionalRecord);
		Assert.isTrue(professionalRecord.getId() > 0);

		this.professionalRecordRepository.delete(professionalRecord);
	}

	// Check ProfessionalRecord
	public void checkProfessionalRecord(final ProfessionalRecord professionalRecord) {
		Boolean res = true;

		if (professionalRecord.getComments() == null)
			res = false;

		Assert.isTrue(res);
	}
}
