
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ComplaintRepository;
import security.Authority;
import domain.Complaint;
import domain.Referee;
import domain.Report;

@Service
@Transactional
public class ComplaintService {

	//Managed Repository------------------------------

	@Autowired
	private ComplaintRepository	complaintRepository;

	//Supporting Services-----------------------------

	@Autowired
	private ActorService		actorService;
	@Autowired
	private RefereeService		refereeService;

	@Autowired
	private ReportService		reportService;


	// Constructor methods ---------------------------------------------------------
	public ComplaintService() {
		super();
	}

	/*
	 * An actor who is authenticated as a customer must be able to:
	 * 1. Manage his or her complaints, which includes listing, showing, and creating them.
	 */

	//lo crea un customer
	public Complaint create() {

		this.actorService.checkAuth(Authority.CUSTOMER);

		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();

		final Complaint res = new Complaint();
		res.setTicker(ticker);

		return res;
	}

	/*
	 * //Se puede borrar si esta modo borrador
	 * public void delete(final Complaint complaint) {
	 * Assert.notNull(complaint);
	 * Assert.isTrue(complaint.getId() != 0);
	 * 
	 * this.actorService.checkAuth(Authority.CUSTOMER);
	 * Assert.notNull(complaint);
	 * 
	 * this.complaintRepository.delete(complaint);
	 * 
	 * }
	 */

	public Complaint findOne(final int complaintId) {
		Assert.isTrue(complaintId != 0);
		final Complaint result = this.complaintRepository.findOne(complaintId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Complaint> findAll() {

		final Collection<Complaint> result = this.complaintRepository.findAll();

		return result;
	}

	public Complaint save(final Complaint complaint) {
		Assert.notNull(complaint);
		final Date moment = new Date();
		this.checkComplaint(complaint);
		Complaint result = new Complaint();
		complaint.setMoment(moment);
		this.actorService.checkAuth(Authority.CUSTOMER);
		this.actorService.checkSpamWords(complaint.getDescription());
		this.actorService.checkSpamWords(complaint.getTicker());

		result = this.complaintRepository.save(complaint);

		return result;
	}

	//Check Complaint
	public void checkComplaint(final Complaint complaint) {
		Boolean result = true;

		if (complaint.getDescription() == null || complaint.getTicker() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Collection<Complaint> complaintsNotSelfAssigned() {
		this.actorService.checkAuth(Authority.REFEREE);
		final Collection<Complaint> result = this.findAll();
		for (final Report r : this.refereeService.findByPrincipal().getReports())
			result.remove(r.getComplaint());
		return result;
	}

	public void assignComplaint(final Complaint c, final Report r) {
		this.actorService.checkAuth(Authority.REFEREE);
		final Referee principal = this.refereeService.findByPrincipal();
		Assert.notNull(c);
		Assert.isTrue(c.getId() != 0);
		Assert.notNull(r);
		Assert.isTrue(r.getId() != 0);

		final Complaint complaint = this.findOne(c.getId());
		final Report report = this.reportService.findOne(r.getId());

		Assert.isTrue(principal.getReports().contains(report));
		report.setComplaint(complaint);

	}

	public Collection<Complaint> getListComplaintSelfAssign() {
		this.actorService.checkAuth(Authority.REFEREE);
		final Referee principal = this.refereeService.findByPrincipal();
		final Collection<Complaint> result = new ArrayList<Complaint>();

		for (final Report r : principal.getReports())
			result.add(r.getComplaint());
		return result;
	}

	Collection<Complaint> findComplaintByCustomer(final int customerId) {
		return this.complaintRepository.findComplaintByCustomer(customerId);
	}

}
