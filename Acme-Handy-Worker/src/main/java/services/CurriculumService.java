
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CurriculumRepository;
import security.Authority;
import domain.Curriculum;
import domain.EducationRecord;
import domain.EndorserRecord;
import domain.MiscellaneousRecord;
import domain.PersonalRecord;
import domain.ProfessionalRecord;

@Service
@Transactional
public class CurriculumService {

	//Managed repository --------------------------------------

	@Autowired
	private CurriculumRepository		curriculumRepository;

	//Supported Services -----------------------------------

	@Autowired
	private ActorService				actorService;

	@Autowired
	private EducationRecordService		educationRecordService;

	@Autowired
	private EndorserRecordService		endorserRecordService;

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;

	@Autowired
	private ProfessionalRecordService	professionalRecordService;

	@Autowired
	private PersonalRecordService		personalRecordService;


	// Contructor methods
	public CurriculumService() {
		super();
	}

	public Curriculum create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);
		final Curriculum res = new Curriculum();

		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();

		final Collection<MiscellaneousRecord> miscellaneousRecords = Collections.<MiscellaneousRecord> emptySet();
		res.setMiscellaneousRecords(miscellaneousRecords);

		final Collection<EndorserRecord> endorserRecords = Collections.<EndorserRecord> emptySet();
		res.setEndorserRecords(endorserRecords);

		final Collection<EducationRecord> educationRecords = Collections.<EducationRecord> emptySet();
		res.setEducationRecords(educationRecords);

		final Collection<ProfessionalRecord> professionalRecords = Collections.<ProfessionalRecord> emptySet();
		res.setProfessionalRecords(professionalRecords);

		final PersonalRecord personalRecord = new PersonalRecord();
		res.setPersonalRecord(personalRecord);

		res.setTicker(ticker);

		Assert.notNull(res);

		return res;
	}

	public Curriculum findOne(final int curriculumId) {
		Assert.isTrue(curriculumId > 0);

		final Curriculum res = this.curriculumRepository.findOne(curriculumId);

		Assert.notNull(res);

		return res;
	}

	public Curriculum save(final Curriculum curriculum) {
		Assert.notNull(curriculum);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		this.checkCurriculum(curriculum);
		this.actorService.checkSpamWords(curriculum.getTicker());

		final Curriculum res = this.curriculumRepository.save(curriculum);

		return res;
	}

	public void delete(final Curriculum curriculum) {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		Assert.notNull(curriculum);
		Assert.isTrue(curriculum.getId() > 0);

		this.curriculumRepository.delete(curriculum);
	}
	public Collection<Curriculum> findAll() {

		Collection<Curriculum> result;

		result = this.curriculumRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Curriculum
	public void checkCurriculum(final Curriculum curriculum) {
		Boolean res = true;

		if (curriculum.getEducationRecords() == null || curriculum.getEndorserRecords() == null || curriculum.getMiscellaneousRecords() == null || curriculum.getPersonalRecord() == null || curriculum.getProfessionalRecords() == null)
			res = false;

		Assert.isTrue(res);
	}
}
