
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ProfileRepository;
import security.LoginService;
import security.UserAccount;
import domain.Profile;

@Service
@Transactional
public class ProfileService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private ProfileRepository	profileRepository;

	// Supporting services ---------------------------------------------------------
	@Autowired
	private ActorService		actorService;


	// Constructor methods ---------------------------------------------------------
	public ProfileService() {
		super();
	}

	//Simple CRUD methods-----------------------------

	public Profile create() {
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		final Profile res = new Profile();
		return res;
	}

	public Profile save(final Profile profile) {
		Assert.notNull(profile);
		Profile result = new Profile();
		//Comprobar que el usuario est� logueado
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		this.actorService.checkSpamWords(profile.getLink());
		this.actorService.checkSpamWords(profile.getNick());
		this.actorService.checkSpamWords(profile.getSocialNetwork());
		result = this.profileRepository.save(profile);
		return result;
	}

	public void delete(final Profile profile) {
		Assert.notNull(profile);
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		Assert.isTrue(this.actorService.findByPrincipal().getProfiles().contains(profile));
		this.profileRepository.delete(profile);
	}

	public Profile findOne(final int profileId) {
		final Profile result = this.profileRepository.findOne(profileId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Profile> findAll() {
		Assert.notNull(LoginService.getPrincipal());
		final Collection<Profile> result = this.profileRepository.findAll();

		return result;
	}

	public boolean exists(final int profileId) {
		return this.profileRepository.exists(profileId);
	}

	//Checkers------------------------------

	//Check Profile
	public void checkProfile(final Profile profile) {
		Boolean result = true;

		if (profile.getNick() == null || profile.getSocialNetwork() == null || profile.getLink() == null)
			result = false;

		Assert.isTrue(result);

	}

}
