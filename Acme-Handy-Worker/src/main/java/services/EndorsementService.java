
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.EndorsementRepository;
import domain.Endorsement;

@Service
@Transactional
public class EndorsementService {

	//Managed Repository------------------------------

	@Autowired
	private EndorsementRepository	endorsementRepository;


	//Supporting Services-----------------------------

	// Constructor methods ---------------------------------------------------------
	public EndorsementService() {
		super();
	}

	public Endorsement create() {

		final Collection<String> comments = new ArrayList<String>();

		final Endorsement res = new Endorsement();
		res.setComments(comments);

		return res;
	}

	public Endorsement save(final Endorsement customerEndorsement) {

		Endorsement result = new Endorsement();
		result = this.endorsementRepository.save(customerEndorsement);

		return result;
	}

	public void delete(final Endorsement endorsement) {
		Assert.notNull(endorsement);
		this.endorsementRepository.delete(endorsement);

	}

	public Endorsement findOne(final int endorsementId) {
		Assert.isTrue(endorsementId != 0);
		final Endorsement result = this.endorsementRepository.findOne(endorsementId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Endorsement> findAll() {
		final Collection<Endorsement> result = this.endorsementRepository.findAll();

		return result;
	}

}
