
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.EducationRecordRepository;
import security.Authority;
import domain.EducationRecord;

@Service
@Transactional
public class EducationRecordService {

	//Managed repository ------------------------------------

	@Autowired
	private EducationRecordRepository	educationRecordRepository;

	//Supported Services -----------------------------------

	@Autowired
	private ActorService				actorService;


	// Contructor methods
	public EducationRecordService() {
		super();
	}

	public EducationRecord create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final EducationRecord res = new EducationRecord();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		return res;
	}

	public Collection<EducationRecord> findAll() {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Collection<EducationRecord> res = this.educationRecordRepository.findAll();

		Assert.notNull(res);

		return res;

	}

	public EducationRecord findOne(final int educationRecordId) {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		Assert.isTrue(educationRecordId > 0);

		final EducationRecord res = this.educationRecordRepository.findOne(educationRecordId);

		Assert.notNull(res);

		return res;
	}

	public EducationRecord save(final EducationRecord educationRecord) {
		Assert.notNull(educationRecord);
		final Date fechaActual = new Date();
		this.actorService.checkAuth(Authority.HANDYWORKER);

		this.checkEducationRecord(educationRecord);
		Assert.isTrue((educationRecord.getStartMoment().before(fechaActual)));
		Assert.isTrue((educationRecord.getStartMoment().before(educationRecord.getEndMoment())));
		this.actorService.checkSpamWords(educationRecord.getAttachment());
		this.actorService.checkSpamWords(educationRecord.getInstitution());
		this.actorService.checkSpamWords(educationRecord.getTitle());

		final EducationRecord res = this.educationRecordRepository.save(educationRecord);

		return res;
	}
	public void delete(final EducationRecord educationRecord) {
		Assert.notNull(educationRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.isTrue(educationRecord.getId() > 0);

		this.educationRecordRepository.delete(educationRecord);
	}

	// Check EducationRecord
	public void checkEducationRecord(final EducationRecord educationRecord) {
		Boolean res = true;
		if (educationRecord.getStartMoment() == null || educationRecord.getEndMoment() == null || educationRecord.getComments() == null)
			res = false;

		Assert.isTrue(res);
	}
}
