
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MessageRepository;
import security.Authority;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Service
@Transactional
public class MessageService {

	//Managed repositories-------------------------------------------
	@Autowired
	private MessageRepository		messageRepository;

	//Supported services---------------------------------------------
	@Autowired
	private FolderService			folderService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private CustomizationService	customizationService;


	//Constructor----------------------------------------------------
	public MessageService() {
		super();
	}

	//Simple CRUD methods--------------------------------------------
	public Message create() {
		final Message res = new Message();
		final Actor sender = this.actorService.findByPrincipal();
		Assert.notNull(sender);
		Assert.notNull(this.actorService.findOne(sender.getId()));
		final Date moment = new Date(System.currentTimeMillis() - 1000);
		final Folder folder = this.folderService.findFolderByNameAndActorId(sender.getId(), "Outbox");

		Assert.notNull(folder);
		res.setSender(sender);
		res.setMoment(moment);
		folder.getMessages().add(res);

		return res;
	}

	public Message findOne(final int messageId) {
		Assert.isTrue(messageId != 0);
		Message res;

		res = this.messageRepository.findOne(messageId);
		Assert.notNull(res);
		return res;
	}

	public Collection<Message> findAll() {
		Collection<Message> res;

		res = this.messageRepository.findAll();
		Assert.notNull(res);
		return res;
	}

	public Message save(final Message message) {
		Assert.notNull(message);
		final Actor sender = message.getSender();
		Assert.notNull(sender);
		final Actor recipient = message.getRecipient();
		Assert.notNull(recipient);
		final Folder f = this.folderService.findFolderByNameAndActorId(sender.getId(), "Outbox");
		Assert.notNull(f);
		f.getMessages().add(message);
		this.folderService.save(f);

		if (this.isSpam(message)) {
			final Folder folder = this.folderService.findFolderByNameAndActorId(recipient.getId(), "Spam");
			Assert.notNull(folder);
			folder.getMessages().add(message);
			this.folderService.save(folder);
		} else {
			final Folder fo = this.folderService.findFolderByNameAndActorId(recipient.getId(), "Inbox");

			Assert.notNull(fo);
			fo.getMessages().add(message);
			this.folderService.save(fo);
		}

		this.actorService.checkSpamWords(message.getBody());
		this.actorService.checkSpamWords(message.getSubject());
		for (final String s : message.getTags())
			this.actorService.checkSpamWords(s);

		return this.messageRepository.save(message);

	}

	public void delete(final Message message) {
		Assert.isTrue(message.getId() != 0);
		final Folder f = this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), "Trash");
		if (f.getMessages().contains(message))
			this.messageRepository.delete(message);
		else {
			final Folder folder = this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), "Trash");
			this.move(message, f);
		}
	}

	//Other methods--------------------------------------------------
	public Folder findFolderByMessage(final Message message) {
		Assert.notNull(message);
		Folder res;
		res = this.messageRepository.findFolderByMessageId(message.getId());
		Assert.notNull(res);
		return res;
	}

	public void move(final Message message, final Folder folder) {
		Assert.notNull(message);
		Assert.notNull(folder);
		final Folder old = this.findFolderByMessage(message);
		Assert.notNull(old);
		Assert.isTrue(this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), old.getName()).getId() == old.getId());
		Assert.isTrue(this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), folder.getName()).getId() == folder.getId());
		folder.getMessages().add(message);
		this.folderService.save(folder);
		old.getMessages().remove(message);
		this.folderService.save(old);
	}

	public Collection<Message> getMessagesByFolder(final Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(folder.getId() == this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), folder.getName()).getId());
		return folder.getMessages();
	}

	public boolean isSpam(final Message message) {
		boolean res = false;
		for (final String spamWord : this.customizationService.getSpamWords()) {
			res = message.getBody().contains(spamWord) || message.getSubject().contains(spamWord);
			if (res)
				break;

		}
		return res;
	}

	public Message broadcast(final Message message) {
		Assert.notNull(message);
		final Actor sender = message.getSender();
		Assert.notNull(sender);
		this.actorService.checkAuth(Authority.ADMIN);

		final Folder senderFolder = this.folderService.findFolderByNameAndActorId(sender.getId(), "Outbox");
		Assert.notNull(senderFolder);
		senderFolder.getMessages().add(message);
		this.folderService.save(senderFolder);

		for (final Actor recipient : this.actorService.findAll())
			if (this.isSpam(message)) {
				final Folder recipientFolder = this.folderService.findFolderByNameAndActorId(recipient.getId(), "Spam");
				Assert.notNull(recipientFolder);
				recipientFolder.getMessages().add(message);
				this.folderService.save(recipientFolder);
			} else {
				final Folder recipientFolder = this.folderService.findFolderByNameAndActorId(recipient.getId(), "Inbox");
				Assert.notNull(recipientFolder);
				recipientFolder.getMessages().add(message);
				this.folderService.save(recipientFolder);
			}

		return this.messageRepository.save(message);
	}
}
