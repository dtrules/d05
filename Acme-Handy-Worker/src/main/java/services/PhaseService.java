
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.PhaseRepository;
import security.Authority;
import domain.Application;
import domain.HandyWorker;
import domain.Phase;
import domain.Task;

@Service
@Transactional
public class PhaseService {

	//Managed repository ------------------------------------------------

	@Autowired
	private PhaseRepository		phaseRepository;

	//Supported Services -------------------------------------------
	@Autowired
	private HandyWorkerService	handyWorkerService;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private ApplicationService	applicationService;

	@Autowired
	private TaskService			taskService;


	// Constructor methods ---------------------------------------------------------
	public PhaseService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Phase create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);
		final Phase res = new Phase();

		return res;
	}
	public Phase save(final Phase phase) {
		Assert.notNull(phase);
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		this.checkPhase(phase);
		final Task task = this.phaseRepository.findTaskByPhase(phase.getId());
		Assert.notNull(task);
		Phase result;

		final Application app = this.applicationService.getApplicationByTask(task.getId());
		if (handyWorker.getApplications().contains(app))
			Assert.isTrue(app.getStatus().getValue() == "ACCEPTED");
		//Multiple phases may overlap, but none of them can be scheduled before or after the period of time during which
		//the corresponding fix-up task must be carried out.
		Assert.isTrue(phase.getStartMoment().after(task.getStartMoment()) || phase.getStartMoment().equals(task.getStartMoment()));
		Assert.isTrue(phase.getEndMoment().before(task.getEndMoment()) || phase.getEndMoment().equals(task.getEndMoment()));
		this.actorService.checkSpamWords(phase.getDescription());
		this.actorService.checkSpamWords(phase.getTitle());
		result = this.phaseRepository.save(phase);

		return result;
	}
	public void delete(final Phase phase) {

		Assert.notNull(phase);

		this.actorService.checkAuth(Authority.HANDYWORKER);
		this.findTaskByPhase(phase.getId());

		this.phaseRepository.delete(phase);

	}
	public Phase findOne(final int phaseId) {
		Assert.isTrue(phaseId != 0);
		Phase result;

		result = this.phaseRepository.findOne(phaseId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Phase> findAll() {

		Collection<Phase> result;

		result = this.phaseRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check phase
	public void checkPhase(final Phase phase) {
		Boolean result = true;

		if (phase.getDescription() == null || phase.getEndMoment() == null || phase.getStartMoment() == null || phase.getTitle() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Task findTaskByPhase(final int phaseId) {
		return this.findTaskByPhase(phaseId);
	}

}
