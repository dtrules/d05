
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TutorialRepository;
import domain.Section;
import domain.Tutorial;
import security.Authority;

@Service
@Transactional
public class TutorialService {

	//Managed repository ------------------------------------------------

	@Autowired
	private TutorialRepository	tutorialRepository;

	@Autowired
	private SectionService		sectionService;

	@Autowired
	private ActorService		actorService;


	// Constructor methods ---------------------------------------------------------
	public TutorialService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Tutorial create() {

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Tutorial t = new Tutorial();

		final Collection<String> pictures = new ArrayList<String>();

		t.setPictures(pictures);

		final Collection<Section> sections = new ArrayList<Section>();

		t.setSections(sections);

		return t;
	}

	public Tutorial save(final Tutorial t) {
		Assert.notNull(t);
		this.checkTutorial(t);

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Date moment = new Date(System.currentTimeMillis() - 1000);

		t.setMoment(moment);

		Tutorial result;
		this.actorService.checkSpamWords(t.getSummary());
		this.actorService.checkSpamWords(t.getTitle());
		for (final String s : t.getPictures())
			this.actorService.checkSpamWords(s);
		result = this.tutorialRepository.save(t);

		return result;
	}

	public void delete(final Tutorial t) {

		Assert.notNull(t);
		Assert.isTrue(t.getId() != 0);

		this.actorService.checkAuth(Authority.HANDYWORKER);

		for (final Section s : t.getSections())
			this.sectionService.delete(s);

		this.tutorialRepository.delete(t);

	}

	public Tutorial findOne(final int tutorialId) {
		Assert.isTrue(tutorialId != 0);
		Tutorial result;

		result = this.tutorialRepository.findOne(tutorialId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Tutorial> findAll() {

		Collection<Tutorial> result;

		result = this.tutorialRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Tutorial
	public void checkTutorial(final Tutorial tutorial) {
		Boolean result = true;

		if (tutorial.getTitle() == null || tutorial.getSummary() == null || tutorial.getPictures() == null || tutorial.getSections() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Tutorial findTutorialBySection(final int sectionId){
		return tutorialRepository.findTutorialBySection(sectionId);
	}

}
