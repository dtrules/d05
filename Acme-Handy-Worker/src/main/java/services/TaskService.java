
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TaskRepository;
import security.LoginService;
import security.UserAccount;
import domain.Application;
import domain.Category;
import domain.Complaint;
import domain.Customer;
import domain.Finder;
import domain.Phase;
import domain.Task;
import domain.Warranty;

@Service
@Transactional
public class TaskService {

	//Managed repository ------------------------------------------------

	@Autowired
	private TaskRepository	taskRepository;

	//Other Services ---------------------------------------------
	@Autowired
	private CustomerService	customerService;
	@Autowired
	private CategoryService	categoryService;

	@Autowired
	private ActorService	actorService;


	// Constructor methods ---------------------------------------------------------
	public TaskService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Task create() {

		UserAccount userAccount;
		final Warranty warranty = new Warranty();
		final Category category = new Category();
		final Collection<Phase> phases = new ArrayList<Phase>();
		final Collection<Complaint> complaints = new ArrayList<Complaint>();
		final Collection<Application> applications = new ArrayList<Application>();
		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();

		userAccount = LoginService.getPrincipal();
		this.customerService.checkIfCustomer();

		final Task res = new Task();

		res.setTicker(ticker);
		res.setComplaints(complaints);
		res.setPhases(phases);
		res.setWarranty(warranty);
		res.setCategory(category);
		res.setApplications(applications);

		return res;
	}

	public Task save(final Task task) {
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		this.customerService.checkIfCustomer();
		this.checkTask(task);
		Task res = new Task();
		Assert.notNull(task);
		final Date moment = new Date(System.currentTimeMillis() - 1000);
		Assert.isTrue(task.getPublishDate().before(moment));
		if (task.getCategory() == null)
			task.setCategory(this.categoryService.findCATEGORY()); //Si no se asigna categoria a task, por defecto se establece la Categoria Padre.
		this.actorService.checkSpamWords(task.getAddress());
		this.actorService.checkSpamWords(task.getDescription());
		res = this.taskRepository.save(task);
		return res;

	}

	public void delete(final Task task) {
		final Collection<Finder> finders = this.finderByTask(task.getId());
		final Customer customer = this.customerByTask(task.getId());
		final Collection<Application> applications = this.applicationByTask(task.getId());

		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		this.customerService.checkIfCustomer();

	}

	public void checkTask(final Task task) {
		Boolean result = true;

		if (task.getAddress() == null || task.getDescription() == null || task.getEndMoment() == null || task.getPublishDate() == null || task.getStartMoment() == null || task.getTicker() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Task findOne(final int taskId) {
		Assert.isTrue(taskId != 0);
		Task result;

		result = this.taskRepository.findOne(taskId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Task> findAll() {

		Collection<Task> result;

		result = this.taskRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Collection<Finder> finderByTask(final int taskId) {
		return this.finderByTask(taskId);
	}

	public Customer customerByTask(final int taskId) {
		return this.customerByTask(taskId);
	}

	public Collection<Application> applicationByTask(final int taskId) {
		return this.applicationByTask(taskId);
	}
}
