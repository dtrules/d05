
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.PersonalRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class PersonalRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private PersonalRecordService	personalRecordService;
	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un PersonalRecord correctamente y guarda datos

	public void createPersonalRecord(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final PersonalRecord personalRecord = this.personalRecordService.create();

			this.personalRecordService.save(personalRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreatePersonalRecord() {

		final Object testingData[][] = {
			// Crear y guardar un PersonalRecord con handy worker autenticado -> true
			{
				"handyWorker1", null
			},
			// Crear y guardar un PersonalRecord con un actor diferente -> false
			{
				"customer1", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createPersonalRecord((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//Caso en el que borramos un personalRecord

	public void deletePersonalRecord(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el educationRecord
			final PersonalRecord personalRecord = (PersonalRecord) this.personalRecordService.findAll().toArray()[0];

			//Borramos
			this.personalRecordService.delete(personalRecord);

			//Comprobamos que se ha borrado

			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	//	@Test
	//	public void driverDeletePersonalRecordTest() {
	//		final Object testingData[][] = {
	//			// Se accede con handyWorker -> true
	//			{
	//				"handyWorker1", null
	//			},
	//			// Se accede con sponsor -> true
	//			{
	//				"sponsor1", IllegalArgumentException.class
	//			},
	//
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.deletePersonalRecord((String) testingData[i][0], (Class<?>) testingData[i][1]);
	//	}
}
