
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.ProfessionalRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ProfessionalRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private ProfessionalRecordService	professionalRecordService;
	@Autowired
	private HandyWorkerService			handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un ProfessionalRecord correctamente y guarda datos

	public void createProfessionalRecord(final String username, final Date startMoment, final Date endMoment, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final ProfessionalRecord professionalRecord = this.professionalRecordService.create();
			professionalRecord.setComments(comments);
			professionalRecord.setEndMoment(endMoment);
			professionalRecord.setStartMoment(startMoment);

			this.professionalRecordService.save(professionalRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateProfessionalRecord() {

		final Date start = new GregorianCalendar(2018, Calendar.OCTOBER, 10).getTime();
		final Date end = new GregorianCalendar(2018, Calendar.OCTOBER, 20).getTime();

		final Date start1 = new GregorianCalendar(2019, Calendar.DECEMBER, 12).getTime();
		final Date end1 = new GregorianCalendar(2019, Calendar.DECEMBER, 20).getTime();

		final Object testingData[][] = {
			// Crear y guardar una ProfessionalRecord con handy worker autenticado
			{
				"handyWorker1", start, end, Collections.<String> emptySet(), null
			},
			// Crear y guardar un ProfessionalRecord con fecha inicio despues de la actual
			{
				"handyWorker2", start1, end1, Collections.<String> emptySet(), IllegalArgumentException.class
			},
			// Crear y guardar una ProfessionalRecord con actor distinto
			{
				"customer1", start, end, Collections.<String> emptySet(), IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createProfessionalRecord((String) testingData[i][0], (Date) testingData[i][1], (Date) testingData[i][2], (Collection<String>) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	//Caso en el que borramos un professionalRecord

	public void deleteProfessionalRecord(final String username, final int professionalRecordId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el professionalRecord
			final ProfessionalRecord professionalRecord = this.professionalRecordService.findOne(professionalRecordId);

			//Borramos
			this.professionalRecordService.delete(professionalRecord);

			//Comprobamos que se ha borrado

			Assert.isNull(this.professionalRecordService.findOne(professionalRecordId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
