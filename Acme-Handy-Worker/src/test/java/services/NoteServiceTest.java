
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import utilities.AbstractTest;
import domain.Note;
import domain.Report;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class NoteServiceTest extends AbstractTest {

	@Autowired
	private NoteService			noteService;
	@Autowired
	private ReportService		reportService;
	@Autowired
	private ActorService		actorService;


	//Caso en que se crea un Note correctamente

	public void createNote(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es referee, customer o handyworker
			actorService.checkAuth(Authority.CUSTOMER, Authority.HANDYWORKER, Authority.REFEREE);

			final Note note = this.noteService.create();

			//this.noteService.save(note);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateReport() {
		
		final Object testingData[][] = {
			// Crear y guardar una note con referee1 autenticado -> true
			{
				"referee1", null
			},
			// Crear y guardar una note con un actor diferente (customer1) -> true
			{
				"customer1", null
			},
			// Crear y guardar una note con un actor diferente (handyWorker1) -> true
			{
				"handyWoker1",  null
			},
			// Crear y guardar una task con un sponsor y con una fecha de publicación posterior a la actual -> false
			{
				"sponsor1", IllegalArgumentException.class
			},
			// Crear y guardar una task con un customer y con una fecha de publicación posterior a la actual -> false
			{
				"customer1",  IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createNote((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//Caso en el que borramos una Note

	public void deleteNote(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el task
			final Note note = (Note) this.noteService.findAll().toArray()[0];

			//Borramos
			this.noteService.delete(note);

			//Comprobamos que se ha borrado

			Assert.isNull(this.reportService.findOne(note.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteNoteTest() {
		final Object testingData[][] = {
				// Se accede con handy worker -> false
				{
						"handyWorker1", null
				},
				// Se accede con referee -> true
				{
						"referee1", null
				},
				// Se accede con customer -> true
				{
						"customer1", null
				},
				// Se accede con sponsor -> false
				{
						"sponsor1", IllegalArgumentException.class
				},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteNote((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
