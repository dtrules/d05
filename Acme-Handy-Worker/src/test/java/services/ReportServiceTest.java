
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Complaint;
import domain.Note;
import domain.Report;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ReportServiceTest extends AbstractTest {

	//Service under test------------------------
	@Autowired
	private ReportService	reportService;

	@Autowired
	private RefereeService	refereeService;

	@Autowired
	private ComplaintService	complaintService;


	//Caso en que se crea un Report correctamente y guarda datos

	public void createReport(final String username, final String description, final Collection<String> attachments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es referee
			this.refereeService.checkIfReferee();
			final Report report = this.reportService.create();
			report.setDescription(description);
			report.setAttachments(attachments);
			report.setComplaint((Complaint) complaintService.findAll().toArray()[0]);
			this.reportService.save(report);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateReport() {

		Collection<String> attachments = new ArrayList<String>();
		attachments.add("Test");

		final Object testingData[][] = {
			// Crear y guardar una report con referee1 autenticado -> true
			{
				"referee1", "Description1", attachments, null
			},
			// Crear y guardar una report con un actor diferente (sponsor2) -> false
			{
				"sponsor2", "Description1", attachments, IllegalArgumentException.class
			},
			// Crear y guardar una report con referee1 autenticado pero atributos inválidos -> true
			{
					"referee1", null, null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createReport((String) testingData[i][0], (String) testingData[i][1],(Collection<String>) testingData[i][2],  (Class<?>) testingData[i][3]);
	}

	//Caso en el que borramos una Report

	public void deleteReport(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el report
			int cont = 0;
			Report report = (Report) this.reportService.findAll().toArray()[cont];


			while(report.getDraftMode()==false){
				cont++;
				report = (Report) this.reportService.findAll().toArray()[cont];

			}

			//Borramos
			this.reportService.delete(report);

			//Comprobamos que se ha borrado

			Assert.isNull(this.reportService.findOne(report.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteReportTest() {
		final Object testingData[][] = {
				// Se accede con handy worker -> false
				{
						"handyWorker1", IllegalArgumentException.class
				},
				// Se accede con referee -> true
				{
						"referee1", null
				},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteReport((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
