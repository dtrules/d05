
package services;

import java.util.Collection;
import java.util.Collections;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Complaint;
import domain.CustomerEndorsement;
import domain.HandyWorker;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class CustomerEndorsementServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CustomerEndorsementService	customerEndorsementService;

	@Autowired
	private CustomerService				customerService;

	@Autowired
	private HandyWorkerService			handyWorkerService;


	//Caso en que se crea un CustomerEndorsement correctamente y guarda datos

	public void createCustomerEndorsement(final String username, final double score, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es customer
			this.customerService.checkIfCustomer();

			final HandyWorker handyWorker = (HandyWorker) this.handyWorkerService.findAll().toArray()[0];

			final CustomerEndorsement customerEndorsement = this.customerEndorsementService.create();
			customerEndorsement.setScore(score);
			customerEndorsement.setHandyWorker(handyWorker);
			customerEndorsement.setComments(comments);
			this.customerEndorsementService.save(customerEndorsement);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateCustomerEndorsement() {

		final Object testingData[][] = {
			// Crear y guardar un CustomerEndorsement con customer autenticado -> true
			{
				"customer1", 32.2, Collections.<Complaint> emptySet(), null
			},
			// Crear y guardar un CustomerEndorsement con un actor diferente (sponsor2) -> false
			{
				"sponsor2", 32.2, Collections.<Complaint> emptySet(), IllegalArgumentException.class
			},
			// Crear y guardar un CustomerEndorsement con un customer con unos comentarios nulos -> true
			{
				"customer1", 32.2, null, null
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createCustomerEndorsement((String) testingData[i][0], (double) testingData[i][1], (Collection<String>) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	//Caso en el que borramos una Task

	public void deleteCustomerEndorsement(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el task
			final CustomerEndorsement customerEndorsement = (CustomerEndorsement) this.customerEndorsementService.findAll().toArray()[0];

			//Borramos
			this.customerEndorsementService.delete(customerEndorsement);

			//Comprobamos que se ha borrado

			//Assert.isNull(this.customerEndorsementService.findOne(customerEndorsement.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverDeleteCustomerEndorsement() {

		final Object testingData[][] = {
			// borrar una task de la base de datos siendo customer -> true
			{
				"customer1", null
			},
			// borrar una task de la base de datos siendo un actor diferente -> false
			{
				"sponsor2", IllegalArgumentException.class
			},
			//borrar una task de un customer que no es suya -> false
			{
				"handyworker1", IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteCustomerEndorsement((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
