
package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Category;
import domain.Task;
import domain.Warranty;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class TaskServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private TaskService		taskService;

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private CategoryService	categoryService;

	@Autowired
	private WarrantyService	warrantyService;


	//Caso en que se crea un Task correctamente y guarda datos

	public void createTask(final String username, final Date publishDate, final Date startMoment, final Date endMoment, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es customer
			this.customerService.checkIfCustomer();
			final Task task = this.taskService.create();
			final Category category = (Category) this.categoryService.findAll().toArray()[0];
			final Warranty warranty = (Warranty) this.warrantyService.findAll().toArray()[0];

			task.setCategory(category);
			task.setEndMoment(endMoment);
			task.setPublishDate(publishDate);
			task.setStartMoment(startMoment);
			task.setWarranty(warranty);
			this.taskService.save(task);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateTask() {

		final Date testPublish = new Date(2018, 10, 1, 10, 0);

		final Date testStart = new Date(2018, 12, 1, 9, 0);
		final Date testEnd = new Date(2018, 12, 30, 10, 0);

		final Date testPublish1 = new Date(2021, 10, 1, 10, 0);
		final Date testStart1 = new Date(2018, 12, 1, 9, 0);

		final Date testEnd1 = new Date(2018, 12, 30, 10, 0);

		final Object testingData[][] = {

			// Crear y guardar una task con customer1 autenticado -> true
			{
				"customer1", testPublish, testStart, testEnd, null
			},
			// Crear y guardar una task con un actor diferente (sponsor1) -> false
			{
				"sponsor2", testPublish, testStart, testEnd, IllegalArgumentException.class
			},
			// Crear y guardar una task con un customer y con una fecha de publicación posterior a la actual -> false
			{
				"customer1", testPublish1, testStart1, testEnd1, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createTask((String) testingData[i][0], (Date) testingData[i][1], (Date) testingData[i][2], (Date) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	//Caso en el que borramos una Task

	public void deleteTask(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el task
			final Task task = (Task) this.taskService.findAll().toArray()[0];
			Assert.notNull(task);
			//Borramos
			this.taskService.delete(task);

			//Comprobamos que se ha borrado

			//			Assert.isNull(this.taskService.findOne(task.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverDeleteTask() {

		final Object testingData[][] = {
			// borrar una task de la base de datos siendo customer -> true
			{
				"customer1", null
			},
			// borrar una task de la base de datos siendo un actor diferente -> false
			{
				"sponsor2", IllegalArgumentException.class
			},
			//borrar una task de un customer que no es suya -> false
			{
				"customer2", IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteTask((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
